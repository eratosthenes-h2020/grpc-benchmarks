package main

import (
	certProto "hello/cert"
	"hello/functions"
	helloProto "hello/hello"
)

// the certificate is added here
func getJWK() *helloProto.JWK {
	certificate := functions.CreateCertificate4Proto()
	JSONwk := &helloProto.JSONWebKey{
		Certificates: []*certProto.Certificate{certificate},
	}

	return &helloProto.JWK{
		JSONWebKey: JSONwk,
	}

}

func createDocForProto() *helloProto.Doc {

	//certificate := functions.CreateCertificate4Proto()

	docVerificationMethod := &helloProto.VerificationMethod{
		Id:         "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		Type:       "Ed25519VerificationKey2018",
		Controller: "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		Value:      [][]byte{{97, 34, 100}},
		//Jwk:               getJWK(),
		RelativeURL:       false,
		MultibaseEncoding: 0,
	}

	docVerificationForAuthentication := &helloProto.Verification{
		VerificationMethod: docVerificationMethod,
		Relationship:       *helloProto.Relationship_Authentication.Enum(),
		Embedded:           false,
	}

	docVerificationForKeyAgreement := &helloProto.Verification{
		VerificationMethod: docVerificationMethod,
		Relationship:       *helloProto.Relationship_KeyAgreement.Enum(),
		Embedded:           false,
	}

	timeForDoc := &helloProto.Timestamp{
		Seconds: 324432,
		Nanos:   2313213,
	}

	processingMetaForDoc := &helloProto.ProcessingMeta{
		BaseURI: "",
	}

	doc := &helloProto.Doc{
		Context:              []string{"https://www.w3.org/ns/did/v1"},
		ID:                   "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		AlsoKnownAs:          nil,
		VerificationMethod:   []*helloProto.VerificationMethod{docVerificationMethod},
		Service:              nil,
		Authentication:       []*helloProto.Verification{docVerificationForAuthentication},
		AssertionMethod:      nil,
		CapabilityDelegation: nil,
		CapabilityInvocation: nil,
		KeyAgreement:         []*helloProto.Verification{docVerificationForKeyAgreement},
		Created:              timeForDoc,
		Updated:              timeForDoc,
		ProcessingMeta:       processingMetaForDoc,
	}

	return doc
}
