syntax = "proto3";;
//package ;

option go_package = "hello/helloProto";


message Message {
	bytes text = 1;
}

message Any {
	string type_url = 1;
	bytes value = 2;
}

message ObjectIdentifier {
	repeated int32 objectIdentifier = 1;
}

message AttributeTypeAndValue {
	
	ObjectIdentifier type = 1;
	Any value = 2;

}

message PkixExtension {
	ObjectIdentifier id = 1;
	optional bool critical = 2;
	repeated bytes value = 3;
}

message PkixName {
	repeated string country = 1;
  	repeated string organization = 2;
  	repeated string organizationalUnit = 3;
  	repeated string locality = 5;
  	repeated string province = 6;
  	repeated string streetAddress = 7;
  	repeated string postalCode = 8;
  	repeated string serialNumber = 9;
  	repeated string commonName = 10;
	
	repeated AttributeTypeAndValue names = 11;
	repeated AttributeTypeAndValue extraNames = 12;
}


// https://github.com/protocolbuffers/protobuf/blob/main/src/google/protobuf/timestamp.proto
// !!! In order to produce time.Time func (x *Timestamp) AsTime() time.Time
// !!! func New(t time.Time) *Timestamp does the opposite 
message Timestamp {
  	int64 seconds = 1;
  	int32 nanos = 2;	
}

message UserInfo {
	string userName = 1;
	string password = 2;
}

// message URL
/*
message URL {
 	string scheme = 1;
 	string opaque = 2;

  	UserInfo user = 3;

  	string host = 4;
  	string path = 5;
  	string rawPath = 6;
  	bool omitHost = 7;
  	bool forceQuery = 8;
  	string rawQuery = 9;
  	string fragment = 10;
  	string rawFragment = 11;
}
*/

message IpAddress {
	repeated bytes iP = 1;
}

message IPNet {
	repeated bytes iP = 1;
	repeated bytes iPMask = 2;	
}

// Defining the x509 certificate
message Certificate {
	repeated bytes raw = 1;
	repeated bytes rawTBSCertficate = 2;
	repeated bytes rawSubjectPublicKeyInfo = 3;

	repeated bytes rawSubject = 4;
	repeated bytes rawIssuer = 5;

	repeated bytes signature = 6;
	int32 signatureAlgorithm = 7; // iota in go 

	int32 publicKeyAlgorithm = 8; // iota in go 
	Any publicKey = 9; // interface{} in go 

	int32 version = 10;
	int64 serialNumber = 11; // big int in go 

	PkixName issuer = 12;
	PkixName subject = 13;

	Timestamp notBefore = 14;
	Timestamp notAfter = 15;
	int32 keyUsage = 16;

	repeated PkixExtension extensions = 17;
	repeated PkixExtension extraExtensions = 18;

	repeated ObjectIdentifier unhandledCriticalExtensions = 19;
	repeated int32 extKeyUsage = 20;

  	repeated ObjectIdentifier unknownExtKeyUsage = 21;

	bool basicConstraintsValid = 22;
  	bool isCA = 23;

  	int32 maxPathLen = 24;
  	bool maxPathLenZero = 25;

  	repeated bytes subjectKeyId = 26;
  	repeated bytes authorityKeyId = 27;

  	repeated string oCSPServer = 28;
  	repeated string issuingCertificateURL = 29;

 	repeated string dNSNames = 30;
  	repeated string emailAddresses = 31;
  	repeated IpAddress iPAddresses = 32;

	repeated URL uRIs = 33;
	
	bool permittedDNSDomainsCritical = 34;
  	repeated string permittedDNSDomains = 35;
  	repeated string excludedDNSDomains = 36;

  	repeated IPNet permittedIPRanges = 37;
  	repeated IPNet excludedIPRanges = 38;

  	repeated string permittedEmailAddresses = 39;
  	repeated string excludedEmailAddresses = 40;
  	repeated string permittedURIDomains = 41;
  	repeated string excludedURIDomains = 42;

  	repeated string cRLDistributionPoints = 43;
	
	repeated ObjectIdentifier policyIdentifiers = 44;
}

// https://github.com/go-jose/go-jose/blob/v3/jwk.go
message JSONWebKey {
	Any key = 1;
	string keyID = 2;	
	string algorithm = 3;
	string use = 4;
	repeated Certificate certificates = 5;
	repeated URL certificatesURL = 6;
	repeated bytes certificateThumbprintSHA1 = 7;
	repeated bytes certificateThumbprintSHA256 = 8;
}

message JWK {
	JSONWebKey jSONWebKey = 1;
	string kty = 2;
	string crv = 3;
}

message VerificationMethod {
	string id = 1;
	string type = 2;
	string controller = 3;
	repeated bytes value = 4;	
	JWK jwk = 5;
	bool relativeURL = 6;
	int32 multibaseEncoding = 7;
}

message Proof {
	string type = 1;
	Timestamp created = 2;
	string creator = 3;
	repeated bytes proofValue = 4;
	string domain = 5;
	repeated bytes nonce = 6;
	string proofPurpose = 7;
	bool relativeURL = 8;
}

message dIDCommV2Endpoint {
	string uRI = 1;
	repeated string accept = 2;
	repeated string routingKeys = 3;
}

message Endpoint {
	repeated dIDCommV2Endpoint rawDIDCommV2 = 1;
	string rawDIDCommV1 = 2;
	Any rawObj = 3;
}

message Service {
	string iD = 1;
	Any type = 2;
	Any priority = 3;

	repeated string recipientKeys = 4;
	repeated string routingKeys = 5;
	Endpoint serviceEndpoint = 6;
	repeated string accept = 7;
	
	map<string, Any> properties = 8;
	map<string, bool> recipientKeysRelativeURL = 9;
	map<string, bool> routingKeysRelativeURL = 10;

	bool relativeURL = 11;
}

message Doc {
	//  must be either a string or a list containing maps and/or strings
	// in our case it will be a string
	Any context = 1; // interface{} in go
	string ID = 2;
	repeated string alsoKnownAs = 3;
	repeated VerificationMethod verificationMethod = 4;

	repeated Service service = 5;

	repeated VerificationMethod authentication = 6;
	repeated VerificationMethod assertionMethod = 7;
	repeated VerificationMethod capabilityDelegation = 8;
	repeated VerificationMethod capabilityInvocation = 9;
	repeated VerificationMethod keyAgreement = 10;
	Timestamp created = 11;
	Timestamp updated = 12;
	repeated Proof proof = 13; 
	
	string baseURI = 14;
}

message ProtocolOperation {
	string operation = 1;
	string protocolVersion = 2;
	int32 transactionNumber = 3;
	int64 transactionTime = 4;
	string type = 5;
	string anchorOrigin = 6;
	string canonicalReference = 7;
	repeated string equivalentReferences = 8;
}

message MethodMetadata {
	string updateCommitment = 1;
	string recoveryCommitmene = 2;
	bool published = 3;
	string anchorOrigin = 4;
	repeated ProtocolOperation unpublishedOperations = 5;
	repeated ProtocolOperation publishedOperations = 6;
}

message DocumentMetadata {
	string versionID = 1;
	bool deactivated = 2;
	string canonicalID = 3;
	repeated string equivalentID = 4;
	MethodMetadata method = 5;
}

message DocResolution {	
	Any context = 1;
	Doc dIDDocument = 2;	
	DocumentMetadata documentMetadata = 3;
}
















