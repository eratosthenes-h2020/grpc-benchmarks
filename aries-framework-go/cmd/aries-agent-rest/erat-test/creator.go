/*
Copyright SecureKey Technologies Inc. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package erat

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"

	//"github.com/google/uuid"
	"strings"

	eratosthenes "erat-test/packages/request"

	"github.com/hyperledger/aries-framework-go/pkg/doc/did"
	//"erat-test/packages/did"
	vdrapi "github.com/hyperledger/aries-framework-go/pkg/framework/aries/api/vdr"
	//vdrapi "erat-test/packages/vdr"
)

const (
	ed25519VerificationKey2018 = "Ed25519VerificationKey2018"
	Bls                        = "Bls12381G1Key2022"
)

// Create builds a new DID Doc.
func (v *VDR) Create(didDoc *did.Doc, opts ...vdrapi.DIDMethodOption) (*did.DocResolution, error) {
	createDIDOpts := &vdrapi.DIDMethodOpts{Values: make(map[string]interface{})}

	// Apply options
	for _, opt := range opts {
		opt(createDIDOpts)
	}

	//check if Doc.id is in the correct syntax form
	idArray := strings.SplitN(didDoc.ID, ":", 4)
	if len(idArray) != 4 || idArray[0] != "did" || idArray[1] != "erat" {
		fmt.Println("id not in expected form")
		return nil, fmt.Errorf("did.id url not in correct form")
	}

	fmt.Println("Before checking genesis hash")

	//check if genesis hash is given correct
	//if idArray[2] != GenesisHash {
	//	fmt.Println("genesisHash given is not as expected")
	//		return nil, fmt.Errorf("genesis hash is not valid")
	//	}

	idArray[2] = GenesisHash

	genesishash := idArray[2]
	publicKey := idArray[3]

	//calculate the identifier
	random_nonce := uuid.NewString()
	h := sha256.New()
	h.Write([]byte(random_nonce + genesishash + publicKey))
	identifier := hex.EncodeToString(h.Sum(nil))

	//replace public key with the identifier
	idArray[3] = identifier

	//create the new did
	newId := ""
	for index, value := range idArray {
		if index != len(idArray)-1 {
			newId = newId + value + ":"
		} else {
			newId = newId + value
		}
	}

	//add the new did to the Doc
	didDoc.ID = newId

	didDocument, err := build(didDoc, createDIDOpts) //TODO Check controller id and add logic for multiple verification methods?
	if err != nil {
		return nil, fmt.Errorf("Create erat DID : %w", err)
	}

	spew.Dump(didDocument)

	var req *eratosthenes.StreamingRawBytes
	req = &eratosthenes.StreamingRawBytes{
		CreateDoc: &eratosthenes.CreateDoc{
			Doc: didDocument,
		},
	}
	schannel <- req

	didDocument.ID = newId //TODO Why is this needed?

	// Consume *did.DocResolution Events from Blockchain
	creator[didDocument.ID] = make(chan *did.DocResolution, maxProcs)
	var docResolution *did.DocResolution
	select {
	case docResolution = <-creator[didDocument.ID]:
		spew.Dump(docResolution)
		delete(creator, didDocument.ID)
		return docResolution, nil
	case <-time.After(time.Second * 20):
		return nil, fmt.Errorf("No Doc Resolution Event received from the grpc-server")
	}
}

func build(didDoc *did.Doc, docOpts *vdrapi.DIDMethodOpts) (*did.Doc, error) {
	if len(didDoc.VerificationMethod) == 0 && len(didDoc.KeyAgreement) == 0 {
		return nil, fmt.Errorf("verification method and key agreement are empty, at least one should be set")
	}

	// VerificationMethods
	mainVM, authentications, assertions, keyAgreements, err := buildDIDVMs(didDoc)
	if err != nil {
		return nil, err
	}

	// Our code goes here
	if len(assertions) == 0 && len(keyAgreements) == 0 {
		keyAgreements = []did.Verification{{
			VerificationMethod: *mainVM,
			Relationship:       did.KeyAgreement,
		}}

		authentications = []did.Verification{{
			VerificationMethod: *mainVM,
			Relationship:       did.Authentication,
		}}
	}

	// Service model to be included only if service type is provided through opts
	//service, err := getServices(didDoc, docOpts)
	//if err != nil {
	//	return nil, err
	//}

	// Created/Updated time
	t := time.Now()

	//verificationMethods := []did.VerificationMethod{*mainVM}

	// I am not using the mainVM
	//did.WithService(service),
	didDoc, err = newDoc(
		didDoc.VerificationMethod,
		did.WithCreatedTime(t),
		did.WithUpdatedTime(t),
		did.WithAuthentication(authentications),
		did.WithAssertion(assertions),
		did.WithKeyAgreement(keyAgreements),
	)
	if err != nil {
		return nil, err
	}

	return didDoc, nil
}

/*
func getServices(didDoc *did.Doc, docOpts *vdrapi.DIDMethodOpts) ([]did.Service, error) {
	if len(didDoc.Service) == 0 {
		return nil, nil
	}

	services := make([]did.Service, len(didDoc.Service))

	for i := range didDoc.Service {
		service, err := configServices(&didDoc.Service[i], &didDoc.VerificationMethod[0], docOpts)
		if err != nil {
			return nil, err
		}

		services[i] = *service
	}

	return services, nil
}

func configServices(service *did.Service, verificationMethod *did.VerificationMethod,
	docOpts *vdrapi.DIDMethodOpts) (*did.Service, error) {
	if service.ID == "" {
		service.ID = uuid.New().String()
	}

	if service.Type == "" && docOpts.Values[DefaultServiceType] != nil {
		v, ok := docOpts.Values[DefaultServiceType].(string)
		if !ok {
			return nil, fmt.Errorf("defaultServiceType not string")
		}

		service.Type = v
	}

	if service.ServiceEndpoint == "" && docOpts.Values[DefaultServiceEndpoint] != nil {
		v, ok := docOpts.Values[DefaultServiceEndpoint].(string)
		if !ok {
			return nil, fmt.Errorf("defaultServiceEndpoint not string")
		}

		service.ServiceEndpoint = v
	}

	if service.Type == vdrapi.DIDCommServiceType {
		didKey, _ := fingerprint.CreateDIDKey(verificationMethod.Value)
		service.RecipientKeys = []string{didKey}
		service.Priority = 0
	}

	return service, nil
}
*/

func buildDIDVMs(didDoc *did.Doc) (*did.VerificationMethod, []did.Verification, []did.Verification, []did.Verification, error) {
	var mainVM *did.VerificationMethod

	//isolate the identifier
	idArray := strings.SplitN(didDoc.ID, ":", 4)

	if len(didDoc.VerificationMethod) != 0 {
		for vMethodIndex, _ := range didDoc.VerificationMethod {
			// TODO remove switch
			//switch verMethod.Type {
			//case ed25519VerificationKey2018, Bls:
			// controller needs to be the same as did.ID
			//didDoc.VerificationMethod[0].Controller = didDoc.ID
			didDoc.VerificationMethod[vMethodIndex].Controller = didDoc.ID

			// change id to include the produced identifier
			//relativeUrlId := didDoc.VerificationMethod[0].ID
			//relativeUrlArray := strings.SplitN(relativeUrlId, ":", 4)
			relativeUrlId := didDoc.VerificationMethod[vMethodIndex].ID
			relativeUrlArray := strings.SplitN(relativeUrlId, ":", 4)

			arrayToBeModified := strings.SplitN(relativeUrlArray[3], "#", 2)
			arrayToBeModified[0] = idArray[3]

			modified := arrayToBeModified[0] + "#" + arrayToBeModified[1]
			relativeUrlArray[3] = modified

			modifiedVerificationID := ""
			for index, value := range relativeUrlArray {
				if index != len(relativeUrlArray)-1 {
					modifiedVerificationID = modifiedVerificationID + value + ":"
				} else {
					modifiedVerificationID = modifiedVerificationID + value
				}
			}

			//didDoc.VerificationMethod[0].ID = modifiedVerificationID
			didDoc.VerificationMethod[vMethodIndex].ID = modifiedVerificationID

			if vMethodIndex == 0 {
				mainVM = did.NewVerificationMethodFromBytes(didDoc.VerificationMethod[0].ID, ed25519VerificationKey2018,
					didDoc.VerificationMethod[0].Controller, didDoc.VerificationMethod[0].Value)
			}

			//default:
			//	return nil, nil, nil, nil, fmt.Errorf("not supported VerificationMethod public key type: %s",
			//		didDoc.VerificationMethod[0].Type)
			//}
		}
	}
	for index := range didDoc.Authentication {
		didDoc.Authentication[index].VerificationMethod.Controller = didDoc.ID
		didDoc.Authentication[index].VerificationMethod.ID = modifyId(didDoc.Authentication[index].VerificationMethod.ID, idArray)
	}

	for index := range didDoc.AssertionMethod {
		didDoc.AssertionMethod[index].VerificationMethod.Controller = didDoc.ID
		didDoc.AssertionMethod[index].VerificationMethod.ID = modifyId(didDoc.AssertionMethod[index].VerificationMethod.ID, idArray)
	}
	for index := range didDoc.KeyAgreement {
		didDoc.KeyAgreement[index].VerificationMethod.Controller = didDoc.ID
		didDoc.KeyAgreement[index].VerificationMethod.ID = modifyId(didDoc.KeyAgreement[index].VerificationMethod.ID, idArray)
	}

	return mainVM, didDoc.Authentication, didDoc.AssertionMethod, didDoc.KeyAgreement, nil
}

func modifyId(vmId string, idArray []string) string {
	relativeUrlArray := strings.SplitN(vmId, ":", 4)

	arrayToBeModified := strings.SplitN(relativeUrlArray[3], "#", 2)
	arrayToBeModified[0] = idArray[3]

	modified := arrayToBeModified[0] + "#" + arrayToBeModified[1]
	relativeUrlArray[3] = modified

	modifiedVerificationID := ""
	for index, value := range relativeUrlArray {
		if index != len(relativeUrlArray)-1 {
			modifiedVerificationID = modifiedVerificationID + value + ":"
		} else {
			modifiedVerificationID = modifiedVerificationID + value
		}
	}

	//didDoc.VerificationMethod[0].ID = modifiedVerificationID
	return modifiedVerificationID
}

func newDoc(publicKey []did.VerificationMethod, opts ...did.DocOption) (*did.Doc, error) {
	if len(publicKey) == 0 {
		return nil, fmt.Errorf("the did:erat  must include public keys")
	}

	// build DID Doc
	doc := did.BuildDoc(append([]did.DocOption{did.WithVerificationMethod(publicKey)}, opts...)...)

	// Create a did doc based on the mandatory value: publicKeys & authentication
	if len(doc.Authentication) == 0 || len(doc.VerificationMethod) == 0 {
		return nil, fmt.Errorf("the did must include public keys and authentication")
	}

	return doc, nil
}
