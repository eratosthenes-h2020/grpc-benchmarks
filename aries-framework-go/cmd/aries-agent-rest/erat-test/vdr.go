/*
Copyright SecureKey Technologies Inc. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package erat

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"

	// GRPC
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"

	// Aries Framework GO
	did "github.com/hyperledger/aries-framework-go/pkg/doc/did"
	//did "erat-test/packages/did"

	// GRPC Bidirectional functions
	grpcEratosthenes "erat-test/grpcEratosthenesAPI"

	// Custom functions and structs
	gobCodec "erat-test/packages/codec"
	eratosthenes "erat-test/packages/request"

	diddoc "github.com/hyperledger/aries-framework-go/pkg/doc/did"
	vdrapi "github.com/hyperledger/aries-framework-go/pkg/framework/aries/api/vdr"
	//vdrapi "erat-test/packages/vdr"
)

var (
	// Keepalive ClientParameters
	kacp = keepalive.ClientParameters{
		Time:                10 * time.Second, // Send pings every 10 seconds if there is no activity
		Timeout:             time.Second,      // Wait 1 second for ping ack before considering the connection dead
		PermitWithoutStream: true,             // Send pings even without active streams
	}

	// Logger
	buf bytes.Buffer //Initialiaze a buffer
	// INFO
	info = func(req, msg string) {
		logger := log.New(&buf, "[INFO] ["+req+"] --> ", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile|log.Lmsgprefix)
		logger.Output(2, msg)
		fmt.Print(&buf)
		buf.Reset()
	}
	// ERROR
	warn = func(req, msg string) {
		logger := log.New(&buf, "[WARN] ["+req+"] --> ", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile|log.Lmsgprefix)
		logger.Output(2, msg)
		fmt.Print(&buf)
		buf.Reset()
	}

	// Declare Receiver Channel and Sender Channel
	maxProcs = runtime.NumCPU() // Find the number of cpus the system has.
	schannel = make(chan *eratosthenes.StreamingRawBytes, maxProcs)
	creator  = make(map[string](chan *did.DocResolution))
	resolver = make(map[string](chan *did.DocResolution))

	//Genesis hash
	GenesisHash string
)

const (
	// DIDMethod did method.
	DIDMethod = "erat"
	// EncryptionKey encryption key.
	EncryptionKey = "encryptionKey"
	// KeyType option to create a new kms key for DIDDocs with empty VerificationMethod.
	KeyType = "keyType"
)

func getenvStr(key string) (string, error) {
	v := os.Getenv(key)
	if v == "" {
		return v, fmt.Errorf("Environment variable is empty")
	}
	return v, nil
}

func getenvBool(key string) (bool, error) {
	s, err := getenvStr(key)
	if err != nil {
		return false, err
	}
	v, err := strconv.ParseBool(s)
	if err != nil {
		return false, err
	}
	return v, nil
}

func receiver(wg *sync.WaitGroup, stream grpcEratosthenes.GrpcEratosthenesEndpoints_ServicesClient) {
	defer wg.Done()

	waitc := make(chan struct{})
	//Reading Data from the server
	go func() {
		for {
			if stream == nil {
				// Stream Failed. Try again TODO
				warn("receiver", "Stream Failed")
				os.Exit(1)
				close(waitc)
				return
			}
			// Receivind Data from the server
			in := new(eratosthenes.StreamingRawBytes)
			err := stream.RecvMsg(in)

			if err == io.EOF {
				// Reading is done.
				close(waitc)
				return
			}

			if err != nil && err != io.EOF {
				warn("receiver", "Failed to Receive Message : "+err.Error())
				stream = nil
				time.Sleep(5 * time.Second)
				//Retry on failure
				continue
			}

			spew.Dump(in)

			switch {
			case in.SubscribeEvent != nil:
				subscription := in.SubscribeEvent
				info("subscriptionEvent", subscription.Event)
				GenesisHash = subscription.GenesisHash
				info("GenesisHash", subscription.GenesisHash)
				//fmt.Println("Setting GenesisHash as an environmental variable...")
				// setting GenesisHash as an environmental variable
				os.Setenv("GenesisHash", subscription.GenesisHash)

			case in.CreateDocEvent != nil:
				createDocEvent := in.CreateDocEvent.DocResolution
				info("createDocEvent", "A CreateDoc Event arrived.")

				creator[createDocEvent.DIDDocument.ID] <- createDocEvent

			case in.ResolveDIDEvent != nil:
				resolveDIDEvent := in.ResolveDIDEvent.DocResolution
				info("resolveDIDEvent", "A ResolveDID Event arrived.")

				resolver[resolveDIDEvent.DIDDocument.ID] <- resolveDIDEvent

			case in.Error != nil:
				fmt.Println("in in.Error")
				err := in.Error
				spew.Dump(err)
				//warn("ERROR", err)
				//TODO:: check the subscribed flag on errors
				//TODO:: Decide what to do with errors and other functions

			default:
				warn("receiver", "Received Unknown GRPC Response...")
			}
		}
	}()
	<-waitc
}

func sender(wg *sync.WaitGroup, stream grpcEratosthenes.GrpcEratosthenesEndpoints_ServicesClient, sch <-chan *eratosthenes.StreamingRawBytes) {
	defer wg.Done()
	for {
		select {
		case x, ok := <-sch:
			if ok {
				data := x

				if err := stream.SendMsg(data); err != nil {
					warn("sender", "Failed to send Request: "+err.Error())
				}
			} else {
				info("sender", "Closing Stream!")
				if err := stream.CloseSend(); err != nil {
					warn("sender", "Failed to close stream: "+err.Error())
				}
				wg.Done()
				return
			}
		}
	}
}

func loadClientTLSCredentials(certFile, keyFile, caFile string, serverHostOverride string) (credentials.TransportCredentials, error) {
	// Load certificate of the CA who signed client's certificate
	pemClientCA, err := ioutil.ReadFile(caFile)
	if err != nil {
		return nil, fmt.Errorf("Failed to read CA certificate")
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemClientCA) {
		return nil, fmt.Errorf("Failed to add CA certificate to the pool")
	}

	// Load server's certificate and private key
	clientCert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, fmt.Errorf("Failed to load X509 Key Pair.")
	}

	// Create the credentials and return it
	config := &tls.Config{
		ServerName:   serverHostOverride,
		Certificates: []tls.Certificate{clientCert},
		RootCAs:      certPool,
	}
	return credentials.NewTLS(config), nil
}

//Path finds absolute path of the current directory and joins the remaining path.
func path(rel string) (string, error) {
	currentPath, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("Failed to retrieve current directory: %v", err)
	}

	if filepath.IsAbs(rel) {
		isExists, err := exists(rel)
		if err != nil {
			return "", err
		}
		if !isExists {
			return "", fmt.Errorf("File does not exists.")
		}
		return rel, nil
	}

	formNewPath := filepath.Join(currentPath, rel)
	isExists, err := exists(formNewPath)
	if err != nil {
		return "", err
	}
	if !isExists {
		return "", fmt.Errorf("File does not exists.")
	}
	return formNewPath, nil
}

func exists(name string) (bool, error) {
	_, err := os.Stat(name)
	if err == nil {
		return true, nil
	}
	if errors.Is(err, os.ErrNotExist) {
		return false, nil
	}
	return false, err
}

// VDR implements did:key method support.
type VDR struct{}

// New returns new instance of VDR that works with did:key method.
func New(vdr *VDR) {
	var err error

	serverAddress, err := getenvStr("GRPC_SERVER_ADDRESS")
	if err != nil {
		warn("new", "GRPC_SERVER_ADDRESS EnvVar not found : "+err.Error())
	}

	serverPort, err := getenvStr("GRPC_SERVER_PORT")
	if err != nil {
		warn("new", "GRPC_SERVER_PORT EnvVar not found : "+err.Error())
	}

	var serverAddr string
	serverAddr = fmt.Sprintf("%s:%s", serverAddress, serverPort)

	serverHostOverride, err := getenvStr("GRPC_SERVER_HOST_OVERRIDE")
	if err != nil {
		warn("new", "GRPC_SERVER_HOST_OVERRIDE EnvVar not found : "+err.Error())
	}

	TLS, err := getenvBool("GRPC_TLS_ENABLED")
	if err != nil {
		warn("new", "GRPC_TLS_ENABLED EnvVar not found : "+err.Error())
	}

	certFile, err := getenvStr("GRPC_TLS_CERT_FILE")
	if err != nil {
		warn("new", "GRPC_TLS_CERT_FILE EnvVar not found : "+err.Error())
	}

	keyFile, err := getenvStr("GRPC_TLS_KEY_FILE")
	if err != nil {
		warn("new", "GRPC_TLS_KEY_FILE EnvVar not found : "+err.Error())
	}

	caFile, err := getenvStr("GRPC_TLS_CA_FILE")
	if err != nil {
		warn("new", "CAGRPC_TLS_CA_FILE EnvVar not found : "+err.Error())
	}

	clientId, err := getenvStr("GRPC_CLIENT_ID")
	if err != nil {
		warn("new", "GRPC_CLIENT_ID EnvVar not found : "+err.Error())
	}

	if !gobCodec.Register() {
		warn("new", "GOB Register map[string]interface")
	}

	info("new", "Number of CPUs: "+strconv.Itoa(maxProcs))
	runtime.GOMAXPROCS(maxProcs)

	if clientId == "" {
		warn("new", "Client ID must be declared before the GRPC Client starts.")
		os.Exit(1)
	}

	var opts []grpc.DialOption
	opts = []grpc.DialOption{
		grpc.WithKeepaliveParams(kacp), // Keepalive Client Parameters
		grpc.WithInitialConnWindowSize(256 * 1024),
		//grpc.WithCodec(&gobCodec.Codec{}), // Custom Codec
	}

	var GRPC string

	if TLS {
		if caFile == "" {
			caFile, err = path("CA/CAcert.pem")
			if err != nil {
				warn("new", "CA File: "+err.Error())
				os.Exit(1)
			}
		}
		if certFile == "" {
			certFile, err = path("TLS/client/clientCert.pem")
			if err != nil {
				warn("new", "Cert File: "+err.Error())
				os.Exit(1)
			}
		}
		if keyFile == "" {
			keyFile, err = path("TLS/client/clientUnencryptedKey.pem")
			if err != nil {
				warn("new", "Key File: "+err.Error())
				os.Exit(1)
			}
		}
		creds, err := loadClientTLSCredentials(certFile, keyFile, caFile, serverHostOverride)
		if err != nil {
			warn("new", "Failed to Generate Credentials: "+err.Error())
			os.Exit(1)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
		GRPC = "GRPCS"
	} else {
		opts = append(opts, grpc.WithInsecure())
		GRPC = "GRPC"
	}

	info("new", "Starting "+GRPC+" client on host "+serverAddr)

	conn, err := grpc.Dial(serverAddr, opts...)
	if err != nil {
		warn("new", "Fail to Dial: "+err.Error())
		os.Exit(1)
	}
	defer conn.Close()

	client := grpcEratosthenes.NewGrpcEratosthenesEndpointsClient(conn)

	info("new", "Client "+clientId+" is connected")

	stream, err := client.Services(context.Background(), grpc.CallCustomCodec(&gobCodec.Codec{}))
	if err != nil {
		warn("new", clientId+".Services(_): "+err.Error())
		os.Exit(1)
	}

	var wg sync.WaitGroup
	wg.Add(2)

	// Magic happens here
	go func() {
		go receiver(&wg, stream)
		go sender(&wg, stream, schannel)
	}()

	fmt.Println("before sending the subscribe request")

	// Create the gRPC StreamingRawBytes request
	var req *eratosthenes.StreamingRawBytes
	req = &eratosthenes.StreamingRawBytes{
		Subscribe: &eratosthenes.Subscribe{
			ClientId: clientId,
		},
	}
	schannel <- req

	wg.Wait()
}

func (v *VDR) GetGenesisHash() string {
	return GenesisHash
}

// Accept accepts did:key method.
func (v *VDR) Accept(method string, opts ...vdrapi.DIDMethodOption) bool {
	return method == DIDMethod
}

// Close frees resources being maintained by VDR.
func (v *VDR) Close() error {
	return nil
}

// Update did doc.
func (v *VDR) Update(didDoc *diddoc.Doc, opts ...vdrapi.DIDMethodOption) error {
	return fmt.Errorf("not supported")
}

// Deactivate did doc.
func (v *VDR) Deactivate(didID string, opts ...vdrapi.DIDMethodOption) error {
	return fmt.Errorf("not supported")
}
