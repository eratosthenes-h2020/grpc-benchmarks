module erat-test

go 1.19

//require github.com/hyperledger/aries-framework-go v0.1.9-0.20221025204933-b807371b6f1e

replace github.com/hyperledger/aries-framework-go => /usr/local/go/src/github.com/hyperledger/aries-framework-go
