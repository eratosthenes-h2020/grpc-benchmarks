package request

import (
	did "github.com/hyperledger/aries-framework-go/pkg/doc/did" // Aries Framework GO
	status "google.golang.org/genproto/googleapis/rpc/status"   // GRPC status
)

type StreamingRawBytes struct {
	Subscribe       *Subscribe       `json:"subscribe,omitempty"`
	SubscribeEvent  *SubscribeEvent  `json:"subscribeEvent,omitempty"`
	CreateDoc       *CreateDoc       `json:"createDoc,omitempty"`
	CreateDocEvent  *CreateDocEvent  `json:"createDocEvent,omitempty"`
	ResolveDID      *ResolveDID      `json:"resolveDID,omitempty"`
	ResolveDIDEvent *ResolveDIDEvent `json:"resolveDIDEvent,omitempty"`
	DisseminateDID  *DisseminateDID  `json:"disseminateDID,omitempty"`
	Error           *status.Status   `json:"error,omitempty"`
}

/* Subscribe Request */
type Subscribe struct {
	ClientId string
}

type SubscribeEvent struct {
	Event       string
	GenesisHash string
}

/* CreateDoc Request */
type CreateDoc struct {
	Doc *did.Doc
}

type CreateDocEvent struct {
	DocResolution *did.DocResolution
}

/* ResolveDID Request */
type ResolveDID struct {
	DocId string
}

type ResolveDIDEvent struct {
	DocResolution *did.DocResolution
}

/* Disseminate to TMB */
type DisseminateDID struct {
	DID string
}
