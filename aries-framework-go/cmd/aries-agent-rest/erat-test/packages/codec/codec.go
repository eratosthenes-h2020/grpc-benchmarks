package codec

import (
	"bytes"
	//"encoding/gob"
	"erat-test/packages/gob"
	"erat-test/packages/request"
	"fmt"
)

// Codec - implements grpc.Codec compatible gob codec.
type Codec struct{}

// our encoder needs to be initializes once
var b bytes.Buffer
var enc = gob.NewEncoder(&b)

// our decoder needs to be initialized once as well
var readerBuffer bytes.Buffer
var decoder = gob.NewDecoder(&readerBuffer)

func Register() bool {
	//gob.Register(map[string]interface{}{})

	gob.Register(request.StreamingRawBytes{})
	return true
}

func (c *Codec) Marshal(v interface{}) ([]byte, error) {
	// var b bytes.Buffer
	// enc := gob.NewEncoder(&b)
	fmt.Println("About to masrshal")

	// reset the buffer
	b.Reset()

	err := enc.Encode(v)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (c *Codec) Unmarshal(b []byte, v interface{}) error {
	fmt.Println("I am about to unmarshal")
	// r := bytes.NewReader(b)
	// dec := gob.NewDecoder(r)
	readerBuffer.Write(b)
	return decoder.Decode(v)
}

// return needs to be the same as the grpc client's gob codec.
func (c *Codec) String() string {
	// return "Custom Marshal-Unmarshal Codec"
	return "gobCodec"
}

// return needs to be the same as the grpc client's gob codec.
func (c *Codec) Name() string {
	return "gobCodec"
}
