package functions

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

func GetTLSConfig(host, caCertFile string) *tls.Config {
	var caCert []byte
	var err error
	var caCertPool *x509.CertPool

	caCert, err = ioutil.ReadFile(caCertFile)
	if err != nil {
		log.Fatal("Error opening cert file", caCertFile, ", error ", err)
	}
	caCertPool = x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	return &tls.Config{
		ServerName: host,
		ClientAuth: tls.RequireAndVerifyClientCert,
		ClientCAs:  caCertPool,
		MinVersion: tls.VersionTLS12,
		CurvePreferences: []tls.CurveID{
			tls.CurveP521,
			tls.CurveP384,
			tls.CurveP256,
		},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
}

/*
cn  : The name of the client who initiated the transaction.
chn : The channel Name
ccn : The chaincode Name
*/

func ConnectToFabricWithWallet(cn string, chn string, ccn string) (*gateway.Contract, error) {
	currentPath, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("Failed to find current path: %s\n", err)
	}

	walletPath := filepath.Join(currentPath, "wallet")
	wallet, err := gateway.NewFileSystemWallet(walletPath)
	if err != nil {
		return nil, fmt.Errorf("Failed to create wallet: %s\n", err)
	}

	if !wallet.Exists(cn) {
		err = populateWallet(wallet)
		if err != nil {
			return nil, fmt.Errorf("Failed to populate wallet contents: %s\n", err)
		}
	}

	ccpPath := filepath.Join(
		currentPath,
		"..",
		"..",
		"..",
		"first-network",
		"connection-org1.json",
	)

	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, cn),
		gateway.WithTimeout(30*time.Second),
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to connect to gateway: %s\n", err)
	}
	defer gw.Close()

	network, err := gw.GetNetwork(chn)
	if err != nil {
		return nil, fmt.Errorf("Failed to get network: %s\n", err)
	}

	contract := network.GetContract(ccn)

	return contract, nil
}

func populateWallet(wallet *gateway.Wallet) error {

	currentPath, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}

	credPath := filepath.Join(
		currentPath,
		"..",
		"..",
		"..",
		"first-network",
		"crypto-config",
		"peerOrganizations",
		"org1.example.com",
		"users",
		"Admin@org1.example.com",
		"msp",
	)

	certPath := filepath.Join(credPath, "signcerts", "Admin@org1.example.com-cert.pem")

	// Read the certificate pem
	cert, err := ioutil.ReadFile(filepath.Clean(certPath))
	if err != nil {
		return err
	}

	privKeyDir := filepath.Join(credPath, "keystore")

	// There's a single file in this dir containing the private key
	files, err := ioutil.ReadDir(privKeyDir)
	if err != nil {
		return err
	}
	if len(files) != 1 {
		return errors.New("Keystore folder should have contain one file")
	}
	privKeyPath := filepath.Join(privKeyDir, files[0].Name())

	err = os.Chmod(privKeyPath, 0777)
	if err != nil {
		return err
	}

	privKey, err := ioutil.ReadFile(filepath.Clean(privKeyPath))
	if err != nil {
		return err
	}

	identity := gateway.NewX509Identity("Org1MSP", string(cert), string(privKey))

	err = wallet.Put("admin", identity)
	if err != nil {
		return err
	}
	return nil
}
