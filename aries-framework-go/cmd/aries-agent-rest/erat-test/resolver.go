package erat

import (
	"fmt"
	"time"

	eratosthenes "erat-test/packages/request"
	"github.com/davecgh/go-spew/spew"

	diddoc "github.com/hyperledger/aries-framework-go/pkg/doc/did"
	//diddoc "erat-test/packages/did"
	vdrapi "github.com/hyperledger/aries-framework-go/pkg/framework/aries/api/vdr"
	//vdrapi "erat-test/packages/vdr"
)

const (
	schemaV1 = "https://w3id.org/did/v1"
	keyType  = "Ed25519VerificationKey2018"
)

func (v *VDR) Read(did string, opts ...vdrapi.DIDMethodOption) (*diddoc.DocResolution, error) {

	fmt.Println("Eimai mesa sth Read")
	fmt.Println("did : ", did)
	parsedDID, err := diddoc.Parse(did)
	fmt.Println("parsedDid : ", parsedDID)
	if err != nil {
		return nil, fmt.Errorf("parsing did failed in indy resolver: (%w)", err)
	}

	if parsedDID.Method != DIDMethod {
		return nil, fmt.Errorf("invalid erat method name: %s", parsedDID.MethodSpecificID)
	}

	resOpts := &vdrapi.DIDMethodOpts{}

	for _, opt := range opts {
		opt(resOpts)
	}

	var req *eratosthenes.StreamingRawBytes
	req = &eratosthenes.StreamingRawBytes{
		ResolveDID: &eratosthenes.ResolveDID{
			DocId: did, //We sent the whole did for now
		},
	}
	schannel <- req

	// Consume *did.DocResolution Events from Blockchain
	resolver[did] = make(chan *diddoc.DocResolution, maxProcs)
	var docResolution *diddoc.DocResolution
	select {
	case docResolution = <-resolver[did]:
		spew.Dump(docResolution)
		delete(resolver, did)
		return docResolution, nil
	case <-time.After(time.Second * 20):
		return nil, fmt.Errorf("No Doc Resolution Event sent")
	}
	return nil, nil
}
