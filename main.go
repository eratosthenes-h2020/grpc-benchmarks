package main

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"os"
	"time"

	helloProto "hello/hello"

	"github.com/davecgh/go-spew/spew"
	did "github.com/hyperledger/aries-framework-go/pkg/doc/did"
	"google.golang.org/protobuf/proto"
)

func publicKey(priv interface{}) interface{} {

	switch k := priv.(type) {

	case *rsa.PrivateKey:
		return &k.PublicKey

	case *ecdsa.PrivateKey:
		return &k.PublicKey

	case ed25519.PrivateKey:
		return k.Public().(ed25519.PublicKey)
	default:
		return nil
	}
}

// initialize encoders and decoders for hello struct
var buf bytes.Buffer
var encoder = gob.NewEncoder(&buf)
var globalDecoder = gob.NewDecoder(&buf)

// initialize encoders and decoders for x509.certificate struct
var certBuf bytes.Buffer
var certEncoder = gob.NewEncoder(&certBuf)
var certDecoder = gob.NewDecoder(&certBuf)

// initialize encoders and decoders for DIDDocument example
var docBuf bytes.Buffer
var didDocumentEncoder = gob.NewEncoder(&docBuf)
var didDocumentDecoder = gob.NewDecoder(&docBuf)

var didDocumentProto = createDocForProto()
var certificate = parseCertificateFromFile()
var didDocumentGob = createDocForGob(certificate)

type hello struct {
	Text []byte
}

var helloVariable = hello{}

var helloExampleStruct = &hello{
	Text: []byte("hello"),
}
var helloExampleProto = &helloProto.Message{
	Text: []byte("hello"),
}

var didDoc = &did.Doc{Context: []string{"https://w3id.org/did/v1"}, ID: "erat:sdjskdjskjdkj"}

func encodeJSON() []byte {
	marshalled, _ := json.Marshal(helloExampleStruct)
	return marshalled
}

func encodeProto() []byte {
	protoBytes, err := proto.Marshal(helloExampleProto)
	if err != nil {
		log.Fatalf("Error in enconding proto hello: %v", err)
	}
	return protoBytes
}

func encodeGobWithInitializedEncoder(v interface{}) {
	//_ = encoder.Encode(helloExampleStruct)
	_ = encoder.Encode(v)

}

func encodeGobWithoutInitializedEncoder() {
	//_ = encoder.Encode(helloExampleStruct)
	var b bytes.Buffer
	var enc = gob.NewEncoder(&b)
	err := enc.Encode(helloExampleStruct)
	if err != nil {
		log.Fatalf("err: %v", err)
	}

}

func encodeJSONCertificate(cert *x509.Certificate) []byte {
	certBytes, err := json.Marshal(cert)
	if err != nil {
		log.Fatalf("err: %v", err)
	}

	return certBytes
}

func decodeJSONCertificate(certificateBytes []byte) {

	err := json.Unmarshal(certificateBytes, &x509.Certificate{})
	if err != nil {
		log.Fatalf("Error in decoding JSON: %v", err)
	}
}
func decodeJSONDoc(docBytes []byte) {

	//err := json.Unmarshal(docBytes, &did.Doc{})
	_, err := did.ParseDocument(docBytes)

	if err != nil {
		log.Fatalf("Error in decoding JSON: %v", err)
	}
}

// Decode hello example struct
func decodeJSON(marshalled []byte) {
	//var m hello
	err := json.Unmarshal(marshalled, &helloVariable)
	if err != nil {
		log.Fatalf("Error in decoding JSON: %v", err)
	}

}

func decodeProto(protoBytes []byte) {
	err := proto.Unmarshal(protoBytes, &helloProto.Message{})
	if err != nil {
		log.Fatalf("Error in decoding proto: %v", err)
	}
}

func decodeProtoDID(docBytes []byte) {
	err := proto.Unmarshal(docBytes, &helloProto.Doc{})
	if err != nil {
		log.Fatalf("Error in decoding proto: %v", err)
	}

}

var didDocExampleProto = helloProto.Doc{}

func decodeProtoDIDDocumentExample(docBytes []byte) {
	err := proto.Unmarshal(docBytes, &didDocExampleProto)
	if err != nil {
		log.Fatalf("Error in decoding proto did document example: %v", err)
	}
}

func decodeGobWithoutIniatializingDecoder(gobBytes []byte) {
	//r := bytes.NewReader(buf.Bytes())
	r := bytes.NewReader(gobBytes)
	dec := gob.NewDecoder(r)
	err := dec.Decode(&helloVariable)
	if err != nil {
		log.Fatalf("err: %v", err)
	}

}

func decodeGobWithInitializedDecoder() {
	err := globalDecoder.Decode(&helloVariable)
	if err != nil {
		log.Fatalf("err: %v", err)
	}
}

func decodeGobCertificateWithoutInitializingDecoder(certBytes []byte) {
	var cert = x509.Certificate{}
	r := bytes.NewReader(certBytes)
	dec := gob.NewDecoder(r)
	err := dec.Decode(&cert)
	if err != nil {
		log.Fatalf("err: %v", err)
	}

}

func decodeGobCertificateWithInitializingDecoder() {
	var cert = x509.Certificate{}
	//err := globalDecoder.Decode(&cert)
	err := certDecoder.Decode(&cert)
	if err != nil {
		log.Fatalf("err: %v", err)
	}
}

var didDocumentExample = did.Doc{}

func decodeGobDIDDocumentExampleWithInitializingDecoder() {
	err := didDocumentDecoder.Decode(&didDocumentExample)
	if err != nil {
		log.Fatalf("err in decoding Gob DID document example with initialized decoder: %v", err)
	}
}

//func decodeGobDIDDocumentExampleWithoutIniitializedDecoder() {
//var doc = did.
//}

func decodeGobDIDDocWithInitializingDecoder() {
	//var cert = x509.Certificate{}
	var doc = did.Doc{}
	//err := globalDecoder.Decode(&cert)
	err := certDecoder.Decode(&doc)
	if err != nil {
		log.Fatalf("err: %v", err)
	}
}

func decodeGobDocWithoutInitializingDecoder(docBytes []byte) {
	//	var cert = x509.Certificate{}
	var doc = did.Doc{}
	r := bytes.NewReader(docBytes)
	dec := gob.NewDecoder(r)
	err := dec.Decode(&doc)
	if err != nil {
		log.Fatalf("err in gob decoding doc: %v", err)
	}

}

func encodeGobCertificateWithoutInitializedEncoder(cert *x509.Certificate) {
	var buf bytes.Buffer
	gob.Register(elliptic.P224())
	gob.Register(elliptic.P256())
	gob.Register(ecdsa.PublicKey{})
	var myEncoder = gob.NewEncoder(&buf)
	//_ = myEncoder.Encode(cert)
	err := myEncoder.Encode(cert)
	if err != nil {
		log.Fatalf("Error in encoding certificate: %v", err)
	}

}

func encodeGobCertificateWithInitializedEncoder(cert *x509.Certificate) {
	//var buf bytes.Buffer
	//var encoder = gob.NewEncoder(&buf)
	//_ = encoder.Encode(cert)
	err := encoder.Encode(cert)
	if err != nil {
		log.Fatalf("gob encode error: %v", err)
	}

}

func encodeProtoDIDForDocument() []byte {
	protoBytes, err := proto.Marshal(didDocumentProto)
	if err != nil {
		log.Fatalf("Error in enconding proto hello: %v", err)
	}
	return protoBytes
}

func encodeProtoDID() []byte {
	//protoBytes, _ := proto.Marshal(&helloProto.Doc{})
	protoBytes, err := proto.Marshal(&helloProto.Doc{})
	if err != nil {
		log.Fatalf("error in Proto encoding DID: %v", err)
	}
	return protoBytes
	//fmt.Println("protobytes length: ", len(protoBytes))
}

func encodeJSONDID() []byte {
	//jsonBytes, _ := didDoc.JSONBytes()
	docBytes, err := didDoc.JSONBytes()
	if err != nil {
		log.Fatalf("error in JSON encoding DID: %v", err)
	}
	return docBytes
	//fmt.Println("jsonBytes length: ", len(jsonBytes))
}

func encodeJSONDIDDocumentExample() []byte {
	documentBytes, err := didDocumentGob.JSONBytes()
	//	_, err := didDocumentGob.JSONBytes()
	if err != nil {
		log.Fatalf("error in JSON encoding DID Document example: %v", err)
	}
	return documentBytes
}

func encodeGobDIDDocumentWithoutInitializedEncoder() {
	var dBuf bytes.Buffer
	var tempEncoder = gob.NewEncoder(&dBuf)
	err := tempEncoder.Encode(didDocumentGob)
	if err != nil {
		log.Fatalf("Error in gob encoding didDocument without initialized encoder: %v", err)
	}
}

// this is the example made by me
func encodeGobDIDDocumentWithInitializedEncoder() {
	err := didDocumentEncoder.Encode(didDocumentGob)
	if err != nil {
		log.Fatalf("error in Gob encoding DID: %v", err)
	}
}

func encodeGobDID() {
	//var buf bytes.Buffer
	//var encoder = gob.NewEncoder(&buf)
	err := encoder.Encode(didDoc)
	if err != nil {
		log.Fatalf("error in Gob encoding DID: %v", err)
	}
	//fmt.Println("gob bytes length: ", len(buf.Bytes()))
}

func parseCertificateFromFile() *x509.Certificate {
	//certFilePath := "/home/fotis/repos/fabric-samples-2.5/fabric-samples/test-network/organizations/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/cacerts/cert.der"
	certFilePath := "./certificate-sample/cert.der"
	certFilePublic, err := os.ReadFile(certFilePath)
	if err != nil {
		panic("err in reading cert file = " + err.Error())
	}
	cert, err := x509.ParseCertificate(certFilePublic)
	if err != nil {
		panic("err in parse cert = " + err.Error())
	}
	return cert
}

func createCertificate() *x509.Certificate {
	priv, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		fmt.Println("panic mode")
	}
	keyUsage := x509.KeyUsageDigitalSignature

	notBefore := time.Now()
	notAfter := time.Now()
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)

	if err != nil {

		log.Fatalf("Failed to generate serial number: %v", err)

	}

	template := x509.Certificate{

		SerialNumber: serialNumber,

		Subject: pkix.Name{

			Organization: []string{"Acme Co"},
		},

		NotBefore: notBefore,

		NotAfter: notAfter,

		KeyUsage: keyUsage,

		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},

		BasicConstraintsValid: true,
	}
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)

	myCertificate, err := x509.ParseCertificate(derBytes)
	if err != nil {
		log.Fatalf("Failed to read certificate %v", err)
	}
	//	fmt.Println("certificate: ", myCertificate)
	//spew.Dump(myCertificate)

	if err != nil {

		log.Fatalf("Failed to create certificate: %v", err)

	}

	return myCertificate

}

//func init() {
//	gob.Register([]interface{}(nil))
//}

func main() {
	//gob.Register(map[string]interface{}{})
	//gob.Register(elliptic.P224())
	//gob.Register(ecdsa.PublicKey{})
	/*
		encodeJSON()
		encodeProto()
		encodeGob()
		encodeProtoDID()
		encodeJSONDID()
		encodeGobDID()
	*/
	// Create an x509 certificate
	priv, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		fmt.Println("panic mode")
	}
	keyUsage := x509.KeyUsageDigitalSignature

	notBefore := time.Now()
	notAfter := time.Now()
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)

	if err != nil {

		log.Fatalf("Failed to generate serial number: %v", err)

	}

	template := x509.Certificate{

		SerialNumber: serialNumber,

		Subject: pkix.Name{

			Organization: []string{"Acme Co"},
		},

		NotBefore: notBefore,

		NotAfter: notAfter,

		KeyUsage: keyUsage,

		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},

		BasicConstraintsValid: true,
	}
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)

	myCertificate, err := x509.ParseCertificate(derBytes)
	if err != nil {
		log.Fatalf("Failed to read certificate %v", err)
	}
	//	fmt.Println("certificate: ", myCertificate)
	//spew.Dump(myCertificate)
	err = encoder.Encode(myCertificate)
	if err != nil {
		log.Fatalf("Failed to create cert: %v", err)
	}

	if err != nil {

		log.Fatalf("Failed to create certificate: %v", err)

	}

	res, _ := proto.Marshal(helloExampleProto)
	spew.Dump(res)

	//encodeJSON()

}
