package main

import (
	"crypto/x509"
	"time"

	did "github.com/hyperledger/aries-framework-go/pkg/doc/did"
)

func createDocForGob(cert *x509.Certificate) did.Doc {

	timeForDoc := time.Now()

	/*
		certFilePath := "/home/fotis/repos/fabric-samples-2.5/fabric-samples/test-network/organizations/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/cacerts/ca.example.com-cert.pem"
		certFilePublic, err := os.ReadFile(certFilePath)
		if err != nil {
			panic("err in reading cert file = " + err.Error())
		}

		certPub, _ := pem.Decode(certFilePublic)
		certPublicKeyCertificate, _ := x509.ParseCertificate(certPub.Bytes)
		certPubKey := certPublicKeyCertificate.PublicKey.(*ecdsa.PublicKey)
	*/

	docVerificationMethod := did.VerificationMethod{
		ID:         "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		Type:       "Ed25519VerificationKey2018",
		Controller: "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		Value:      []byte{97, 34, 100},
		//JsonWebKey: &jwk.JWK{},
		//&jose.JSONWebKey{
		//	Certificates: []*x509.Certificate{cert},
		//},
	}

	//docVerificationMethod.JsonWebKey.JSONWebKey =
	/*
		jose.JSONWebKey{
			Key:          certPubKey,
			Certificates: []*x509.Certificate{cert},
		}
	*/

	verificationForDoc := did.Verification{
		VerificationMethod: docVerificationMethod,
		Relationship:       did.Authentication,
		Embedded:           false,
	}

	doc := did.Doc{
		Context:              []string{"https://www.w3.org/ns/did/v1"},
		ID:                   "did:erat:9bca45e71dbc6f1665321dbf62a575aa2071ca602015df0b085baca7c44881b7:bf385d4a82d6ef859b440befd92d7351147d716d93e0e9bf9b01a34575f6d536",
		AlsoKnownAs:          nil,
		VerificationMethod:   []did.VerificationMethod{docVerificationMethod},
		Service:              nil,
		Authentication:       []did.Verification{verificationForDoc},
		AssertionMethod:      nil,
		CapabilityDelegation: nil,
		CapabilityInvocation: nil,
		KeyAgreement:         []did.Verification{verificationForDoc},
		Created:              &timeForDoc,
		Updated:              &timeForDoc,
		Proof:                nil,
	}

	return doc

}
