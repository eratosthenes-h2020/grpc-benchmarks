package functions

import (
	certProto "hello/cert"
	"log"

	"google.golang.org/protobuf/proto"
)

func DecodeProtoCertificateExample(certBytes []byte) {
	err := proto.Unmarshal(certBytes, &certProto.Certificate{})
	if err != nil {
		log.Fatalf("Error in decoding proto x509 certificate example: %v", err)
	}
}
