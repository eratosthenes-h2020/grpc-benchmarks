package functions

import (
	"crypto/rand"
	certProto "hello/cert"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func GenRandomBytes(size int) (blk []byte, err error) {
	blk = make([]byte, size)
	_, err = rand.Read(blk)
	return
}

func ProduceTimeStamp() *timestamppb.Timestamp {
	return &timestamppb.Timestamp{
		Seconds: 1263786698,
		Nanos:   1263786698,
	}
}

func GetExtensions() []*certProto.PkixExtension {
	PkixExtension := &certProto.PkixExtension{
		Id:       []int32{134343343, 32432, 3434, 3233223},
		Critical: true,
		Value:    []byte{04, 20, 89, 47, 65, 59, 23, 2, 123, 73, 32, 23, 42, 91, 51, 69, 04, 20, 89, 47, 65, 59, 23, 2, 123, 73, 32, 23, 42, 91, 51, 69, 45, 45},
	}

	return []*certProto.PkixExtension{PkixExtension, PkixExtension, PkixExtension}

}

func GetSubjectKeyId() []byte {
	return []byte{04, 20, 89, 47, 65, 59, 23, 2, 123, 73, 32, 23, 42, 91, 51, 69, 04, 20, 89, 47, 65, 59}
}

func IssuerPKIX() *certProto.PkixName {

	return &certProto.PkixName{
		Country:            []string{"Monaco,Athens"},
		Organization:       []string{"Hyperledger"},
		OrganizationalUnit: []string{},
		Locality:           []string{},
		Province:           []string{},
		StreetAddress:      []string{"ConorStreet"},
		PostalCode:         []string{"12234"},
		SerialNumber:       "34324832948032",
		CommonName:         "ca.example.com",
		Names: &certProto.AttributeTypeAndValue{
			Type:  "random",
			Value: []byte("random"),
		},
		ExtraNames: &certProto.AttributeTypeAndValue{
			Type:  "random",
			Value: []byte("random"),
		},
	}

}

//func initPublicKeyAlgorithm()

func CreateCertificate4Proto() *certProto.Certificate {
	certRaw, err := GenRandomBytes(577)
	if err != nil {
		panic(err)
	}
	certRawTBSCertificate, err := GenRandomBytes(487)
	if err != nil {
		panic(err)
	}
	certRawSubjectPublicKeyInfo, err := GenRandomBytes(91)
	if err != nil {
		panic(err)
	}
	certRawSubject, err := GenRandomBytes(107)
	if err != nil {
		panic(err)
	}
	certRawIssuer, err := GenRandomBytes(107)
	if err != nil {
		panic(err)
	}
	certSignature, err := GenRandomBytes(71)
	if err != nil {
		panic(err)
	}
	certSignatureAlgorithm := certProto.SignatureAlgorithm_ECDSAWithSHA256
	certPublicKeyAlgorithm := certProto.PublicKeyAlgorithm_ECDSA
	certPublicKey := &certProto.ECDSAPublicKey{
		Curve: &certProto.Curve{
			P:    2,
			N:    3434,
			B:    3424324234324,
			Gx:   3424324324,
			Gy:   3432432432432432,
			Name: "Fotis",
		},
		X: 34343432123,
		Y: 231321312839,
	}

	certificate := &certProto.Certificate{
		Raw:                     certRaw,
		RawTBSCertificate:       certRawTBSCertificate,
		RawSubjectPublicKeyInfo: certRawSubjectPublicKeyInfo,
		RawSubject:              certRawSubject,
		RawIssuer:               certRawIssuer,
		Signature:               certSignature,
		SignatureAlgorithm:      certSignatureAlgorithm,
		PublicKeyAlgorithm:      certPublicKeyAlgorithm,
		PublicKey:               certPublicKey,
		Version:                 3,
		SerialNumber:            96867587291289324,
		Issuer:                  IssuerPKIX(),
		Subject:                 IssuerPKIX(),
		NotBefore:               ProduceTimeStamp(),
		NotAfter:                ProduceTimeStamp(),
		KeyUsage:                1,

		ExtKeyUsage:                 []certProto.ExtKeyUsage{23},
		UnknownExtKeyUsage:          []int32{34, 3434, 343, 434},
		Extensions:                  GetExtensions(),
		ExtraExtensions:             GetExtensions(),
		UnhandledCriticalExtensions: nil,
		BasicConstraintsValid:       true,
		IsCA:                        true,
		MaxPathLen:                  -1,
		MaxPathLenZero:              false,
		SubjectKeyId:                GetSubjectKeyId(),
		AuthorityKeyId:              nil,
	}

	return certificate

}
