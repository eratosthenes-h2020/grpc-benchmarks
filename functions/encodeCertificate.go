package functions

import (
	certProto "hello/cert"
	"log"

	"google.golang.org/protobuf/proto"
)

func EncodeCertificate4Proto(cert *certProto.Certificate) []byte {
	certificateBytes, err := proto.Marshal(cert)
	if err != nil {
		log.Fatalf("Error in encoding certificate 4 proto: %v", err)
	}
	return certificateBytes
}
