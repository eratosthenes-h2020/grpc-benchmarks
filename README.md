# Benchmarks 

## Table of Contents
- [Description](#description)
- [Structures Used](#structures-used)
- [Prerequisites](#prerequisites)
- [Protocol Buffers](#protocol-buffers)
- [JSON](#json)
- [Gob](#gob)
- [DID Document Structure Initialization](#did-document-structure-initialization)
- [Certificate Structure Initialization](#certificate-structure-initialization)
- [Execution](#execution)
- [Benchmarks Excel](#benchmarks-excel)


## Description 

Our goal was to measure the performance of serializing and deserializing data using different codecs in terms of time and memory.<br>
The codecs that were used are 

- Protocol Buffers
- JSON 
- Gob without an initialized encoder/decoder
- Gob with an initialized encoder/decoder 

### Structures used

- ````
    type Hello struct {
        Text []byte
    } 

- [x509.Certificate](https://pkg.go.dev/crypto/x509#Certificate)

- [DID Document](https://github.com/hyperledger/aries-framework-go/blob/v0.1.8/pkg/doc/did/doc.go#L259)

- protofiles 

### Prerequisites
In order to run the benchmarks described `go v1.17` needs to be installed.

### Protocol Buffers

- `Doc` message that contains the DID document fields in the `didDoc-with-cert.proto` file.
- `Message` message that contains the Hello struct fields in the `didDoc-with-cert.proto`
- `Certificate` message contains the x509 Certificate ported from the x509 `go 1.17.2` package located in the `/cert/cert.proto` file 

### JSON 

For the encoding and decoding of the DID document struct we used the aries-framework go encoding and decoding functions 

- [encoding Doc](https://github.com/hyperledger/aries-framework-go/blob/v0.1.8/pkg/doc/did/doc.go#L465)

- [decoding Doc](https://github.com/hyperledger/aries-framework-go/blob/v0.1.8/pkg/doc/did/doc.go#L1075) 

### Gob 

For encoding/decoding we used two different ways 

- encoding
    - initialization of an encoder
    - without initialization of a decoder

- decoding
    - initialization of a decoder
    - without initialization of a decoder

### DID Document Structure Initialization
- [DID Document Initialization for gob encoding](doc-example-for-gob.go)
- [DID Document Initialization for protocol buffers](doc-example-for-proto.go)

### Certificate Structure Initialization
- `parseCertificateFromFile` function in main.go
    An x509 certificate from a Hyperledger Fabric network is used as a sample.
- [Certificate Initialization for protocol buffer](/functions/createCertificate.go)
    The certificate is initialized based on the Hyperledger's Fabric certificate mentioned above.

The certificate mentioned above is located under the `/certificate-sample directory` in a der format.

### Execution 
Benchmarks run using the following command:

```
go test -bench=. -benchmem -benchtime=500x
```

For each benchmark the number of iterations is 500
Below an image from the execution is attached

![benchmarks-finalized.png](benchmarks-finalized.png)

### Benchmarks excel: 


- [DID Document benchmarks with no certificate integrated](gRPCBenchmarks-DID-no-cert.xlsx) where the DID Document's jsonWebKey is nil and it does not contain an x509 certificate.

- [DID Document benchmarks with a x509 certificate integrated](gRPCBenchmarks-DID-with-cert.xlsx) DID Document's jsonWebKey contains the x509 certificate.

